@extends('layouts.master')

@section('content')
    <div class="centered">
        {{--<p>--}}
            {{--Scrape popcorn thoroughly, then mix with ice water and serve tenderly in pan.--}}
            {{--Malfunction wihtout metamorphosis, and we won’t offer an astronaut.--}}
            {{--Burn me tuna, ye proud bilge rat! The silence of your mans will balance qabalistic when you witness that beauty is the body.--}}
            {{--Loot me furner, ye fine shore! Eggs taste best with whipped cream and lots of nutmeg.--}}
        {{--</p>--}}
        {{--<ul>--}}
            {{--@for($i=0; $i<5; $i++)--}}
                {{--@if($i % 2 === 0)--}}
                    {{--<li>Iteration {{$i+1}}</li>--}}
                {{--@endif--}}
            {{--@endfor--}}
        {{--</ul>--}}

        @foreach($actions as $action)
            <a href="{{route('niceaction', ['action' => lcfirst($action->name)])}}">{{$action->name}}</a>
        @endforeach
        <br>
        <br>
        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach($errors->all() as $error)
                        {{$error}}
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{route('add_action')}}" method="post">
            <label for="name">Name of action</label>
            <input type="text" name="name" />
            <label for="niceness">Niceness level</label>
            <input type="text" name="niceness" />
            <button type="submit">Do a nice action!</button>
            <input type="hidden" value="{{Session::token()}}" name="_token">
        </form>
        <br><br><br>
        <ul>
            @foreach($logged_actions as $logged_action)
                <li>
                    {{ $logged_action->nice_action->name }}
                    @foreach($logged_action->nice_action->categories as $category)
                    {{ $category->name }}
                    @endforeach
                </li>
            @endforeach
        </ul>
    </div>
@endsection