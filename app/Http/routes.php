<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware'=>['web']], function (){
    Route::get('/', [
        'uses' => 'NiceActionController@getHome',
        'as' => 'home'
    ]);
});



Route::group(['prefix'=>'do'], function (){
    Route::get('/{action}/{name?}',[
        'uses' => 'NiceActionController@getNiceAction',
        'as' => 'niceaction'     //This is similar to the ->name('name') at the end of route declaration
    ]);

//    Route::get('/pray/{name?}', function ($name=null) {
//        return view('actions.pray', ['name'=>$name]);
//    })->name('pray');
//
//    Route::get('/welcome/{name?}', function ($name=null) {
//        return view('actions.welcome', ['name'=>$name]);
//    })->name('welcome');

    Route::post('/add_action', [
        'uses' => 'NiceActionController@postInsertNiceAction',
        'as' => 'add_action'
    ]);
});



